drop
database if exists backage;
create
database backage;

use backage;

create table customer(
	id int auto_increment primary key,
    fullname varchar(30) not null,
    username varchar(30) not null,
    email varchar(50) not null,
    address varchar(80) not null,
    pass varchar(128) not null,
    unique(email)
);

create table administrator(
	id int auto_increment primary key,
	username varchar(40) not null,
    pass varchar(100) not null,
    fullname varchar(200) not null
);

create table product
(
	id int auto_increment primary key,
    descr varchar(300) not null,
	price int not null,
    size int not null,
    image varchar(100) not null
);

create table orders
(
	id int auto_increment primary key,
	idadmin int,
	iduser int not null,
	stat varchar(30) not null, 
    datareg varchar(30) not null,
    address varchar(80) not null,
    foreign key(idadmin) references administrator(id),
    foreign key(iduser) references customer(id)
);

create table ordersdetails
(
	id int auto_increment primary key,
    idorders int not null,
    idprod int not null,
    descrizione varchar(300) not null,
    quantity int not null,
    price float not null,
	foreign key(idorders) references orders(id),
    foreign key(idprod) references product(id)
);
    


create table size
(
	id int auto_increment primary key,
    descr varchar(10) not null
);

create table cart
(
	id int auto_increment primary key,
    idprod int not null,
    quantity int not null,
    iduser int not null,
	foreign key(idprod) references product(id),
    foreign key(iduser) references customer(id)
);



use backage;

insert into customer(fullname, username, email, pass, address) values
("Mario Mario","supermario", "mario01@gmail.com","Scdfg54", "Via dei Matti numero 0"),
("Yoshi Uovo","yoshi", "marco22@libero.it","ciao43", "Via Nizza 34"),
("Luigi Mario","luigi", "luigi34@gmail.com","HelloWorld", "Piazza Grande 44");


insert into administrator(username,pass,fullname) values
("paracetaa","123admin","Aldo Mustaro");

insert into product(price,descr,size,image) values
("20","Maglia Tie Dye Arcobaleno","4","immagini/magliaverde.jpg"),
("40", "Felpa Vintage Anni 80","3","immagini/magliaverde.jpg"),
("20","Maglia Tie Dye Arcobaleno","2","immagini/magliaverde.jpg"),
("20","Maglia Tie Dye Arcobaleno","1","immagini/magliaverde.jpg");

insert into size(descr) values
("S"),("M"),("L"),("XL"),("XXL");

SELECT *
FROM administrator;

