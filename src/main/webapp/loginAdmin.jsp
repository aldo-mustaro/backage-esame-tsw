<%@ page contentType="text/html; text/css; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="css/loginRegistrazione.css" />
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>


<body>
<div class="login-box">
    <h2>Login Admin</h2>
    <form action="login">
        <div class="user-box">
            <input type="text" id="username" name="username" autocomplete="nope">
            <label>Username</label>
        </div>
        <div class="user-box">
            <input type="password" id="password" name="psw" autocomplete="new-password">
            <label>Password</label>
        </div>
        <a href="#">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <input type="submit" value="AccediAdmin" name="accediAdmin" id="accediAdmin">
            <input type="submit" value="Sei un nuovo utente? Registrati" name="registrati" id="registrati">
            <input type="submit" value="Home" name="home" id="home">
        </a>


    </form>
</div>


<script  src="./script.js"></script>

</body>
</html>

