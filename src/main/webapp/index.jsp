<%@ page import="Model.User" %>
<%@ page import="DAO.productDAO" %>
<%@ page import="Model.product" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial scale=1.0">
    <link rel="stylesheet" href="css/homepage.css" type="text/css" />
    <script src="//code.jquery.com/jquery.min.js"></script>
    <title>Backage</title>
</head>


<body>

    <div id="nav-placeholder">
    </div>



<div class="row">
        <div class="col-2">
            <h1> Capi dal passato <br> resi nuovi </h1>
        </div>
        <div class="col-2">
            <img src="immagini/people.png">
        </div>

</div>
</div>



    <!-------featured products ------>
    <div class="small-container">
        <h2 class="titleProduct">Ultimi prodotti</h2>

        <div class="row">
            <%
                productDAO pDAO=new productDAO();
                List<product> listaProd=pDAO.doRetrieveLatest();
                for (product prodotto : listaProd) {
            %>
            <div class="col-4">
                <form action="articoli">
                    <button id="articoli" name="articoli">
                    <img src="<%=prodotto.getImage()%>" alt="maglia"> </button>
                </form>

                <h4><%=prodotto.getDesc()%></h4>
                <p>€<%=prodotto.getPrice()%></p>
            </div>
            <% } %>
        </div>
    </div>


    <div id="footer-placeholder">

    </div>

<!---------------js for toggle menu------------------->
<script>
    var MenuItems=document.getElementById("MenuItems");

    function menutoggle(){
        if(MenuItems.style.maxHeight=="0px"){
            MenuItems.style.maxHeight="200px";
        } else{
            MenuItems.style.maxHeight="0px";
        }
    }

    $(function(){
        $("#nav-placeholder").load("navbar.jsp");
    });

    $(function(){
        $("#footer-placeholder").load("footer.jsp");
    });


</script>

</body>

</html>

