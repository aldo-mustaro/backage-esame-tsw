<%@ page import="Model.product" %>
<%@ page import="java.util.List" %>
<%@ page import="DAO.productDAO" %>
<%@ page import="Model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/articoli.css" type="text/css" />
    <script src="//code.jquery.com/jquery.min.js"></script>
    <title>ARTICOLI</title>
</head>
<body>

    <div id="nav-placeholder">
    </div>


        <div class="small-container">
            <h2 class="titleProduct">Prodotti</h2>

            <div class="row">
                <%
                    List<product> prodotti=(List<product>) request.getAttribute("prodottoList");
                    for (product prodotto : prodotti) {
                %>
                <div class="col-4">
                    <form action="articoli">
                        <img src="<%=prodotto.getImage()%>" alt="maglia">
                        <button class="addToCart" id="goTo" name="goTo" value="<%=prodotto.getDesc()%>"> AGGIUNGI AL CARRELLO
                             </button>
                    </form>

                    <h4><%=prodotto.getDesc()%></h4>
                    <p>€<%=prodotto.getPrice()%></p>

                </div>
                <% } %>
            </div>
        </div>



<div id="footer-placeholder">
</div>



<script>

    $(function(){
        $("#nav-placeholder").load("navbar.jsp");
    });


    $(function(){
        $("#footer-placeholder").load("footer.jsp");
    });
</script>





</body>
</html>
