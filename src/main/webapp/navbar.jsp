<%@ page import="DAO.productDAO" %>
<%@ page import="Model.product" %>
<%@ page import="java.util.List" %>
<%@ page import="Model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <meta charset="UTF-8">
    <style>


        .logo img{
            width: 350px;
            display: block;
            margin-left: auto;
            margin-right: auto;
        }


        .navbar {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #333;
            box-shadow: 0 15px 25px rgba(0,0,0,.6);
            position:sticky;
        }

        .navbar li {
            float: left;
        }

        .navbar li a{
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;

        }

        .navbar li a:hover {
            background-color: #111;
        }

        .articoli{
            background-color: transparent;
            background-repeat: no-repeat;
            border: none;
            cursor: pointer;
            overflow: hidden;
            outline: none;
            padding:22px;
            size:auto;
            color: #fff;
        }

        .logOut{
            background-color: transparent;
            background-repeat: no-repeat;
            border: none;
            cursor: pointer;
            overflow: hidden;
            outline: none;
            padding:15px;
            color: #fff;
            text-align: right;
        }

        .articoli:hover{
            background-color: #111111;
        }

        .cart:hover{
            background-color: #111111;
        }

        .logOut2:hover{
            background-color: #111111;
        }

        .cart {
            background-color: transparent;
            background-repeat: no-repeat;
            border: none;
            cursor: pointer;
            overflow: hidden;
            outline: none;
            padding:2px;
        }

        .menu-icon{
            width:28px;
            margin-left: 20px;
            background-color: transparent;
            background-repeat: no-repeat;
            border: none;
            cursor: pointer;
            overflow: hidden;
            outline: none;
            padding:6px;
            text-align: right;
            display:none;
        }



        @media only screen and (max-width: 800px){

            .navbar ul{
                display:none;
                position: absolute;
                top: 70px;
                left: 0;
                background: #333;
                width: 100px;
                overflow: hidden;
                transition: max-height 0.5s;

            }

            .navbar ul li{
                display: block;
                margin-right: 50px;
                margin-top: 10px;
                margin-bottom: 10px;

            }

            .navbar ul li a{
                color: #fff;
            }
            .menu-icon{
                display: block;
                cursor: pointer;
            }
        }



        @media only screen and (max-width: 600px){
            .row{
                text-align:center;
            }
            .col-2, .col-3, .col-4{
                flex-basis:100%;
            }

        }

        .log{
            float:right!important;
            flex:1;
            text-align: right;
            display: inline-block;
            margin-right:20px;
        }

    </style>


</head>
<body>


<!-- Menu' -->
<div class="titolo">
    <div class="logo">
        <a href="index.jsp">
        <img src="immagini/logo2.png"> </a>
    </div>
</div>


<div class="mainHeader">

    <ul class="navbar" id="MenuItems">

        <form action="homepage">
            <li><button class="articoli" type="submit" name="articoli" value="articoli"> Articoli</button></li>
        </form>


        <form action="homepage">
            <li class="log"><button class="cart" type="submit" name="carrello" value="carrello">
                <img src="immagini/cart.png" alt="carrello" width="75px"></button></li>
        </form>

        <li class="log"><a href="account.jsp" id="account" name="account" value="account">Account</a></li>


        <%  session = request.getSession(false);
            User u = (User) session.getAttribute("utente");
            if(u!=null){
        %>
        <li class="log"><a href="account.jsp"><%= u.getUsername()%></a></li>
        <form action="account">
            <li class="log"><button class="logOut" type="submit" name="logOut" value="logOut"> Log Out</button></li>
        </form>

        <% } else { %>

        <li class="log"><a href="login.jsp">Login</a></li>
        <form action="homepage">
            <li class="log"><button class="logOut" type="submit" name="registrazione" value="registrati"> Registrati </button></li>
        </form>
        <% } %>



    </ul>
    <li class="log"> <img src="immagini/menu.jpg" class="menu-icon" onclick="menutoggle()" > </li>


</div>

<br>


<script>
    var MenuItems=document.getElementById("MenuItems");

    function menutoggle(){
        if(MenuItems.style.maxHeight=="0px"){
            MenuItems.style.maxHeight="200px";
        } else{
            MenuItems.style.maxHeight="0px";
        }
    }
</script>

</body>
</html>
