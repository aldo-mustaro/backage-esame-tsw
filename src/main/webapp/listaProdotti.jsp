<%@ page import="DAO.productDAO" %>
<%@ page import="Model.product" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Sharp" rel="stylesheet">
    <link rel="stylesheet" href="css/adminView.css">
    <title>Title</title>
</head>
<body>
<div class="container">
    <aside>
        <div class="top">
            <div class="logo">
                <a href="index.jsp"></a>
                <img src="immagini/logo2.png">
                </a>
            </div>
            <div class="close" id="close-btn">
                <span class="material-icon-shard">close</span>
            </div>
        </div>

        <div class="sidebar">
            <a href="homepageAdmin.jsp">
                <span class="material-icons-sharp">grid_view</span>
                <h3>Dashboard</h3>
            </a>
            <a href="listaClienti.jsp">
                <span class="material-icons-sharp">person_outline</span>
                <h3>Customers</h3>
            </a>
            <a href="listaOrdini.jsp">
                <span class="material-icons-sharp">receipt_long</span>
                <h3>Orders</h3>
            </a>
            <a href="listaProdotti.jsp">
                <span class="material-icons-sharp">inventory</span>
                <h3>Products</h3>
            </a>


            <a href="#">
                <span class="material-icons-sharp">logout</span>
                <h3>Log Out</h3>
            </a>

        </div>
    </aside>



    <main>
        <div class="recent-order">
            <h2>LISTA PRODOTTI</h2>
            <table>
                <thead>
                <tr>
                    <th> Id prodotto </th>
                    <th> Descrizione prodotto </th>
                    <th></th>
                    <th> Taglia </th>
                    <th></th>
                    <th> Prezzo </th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                <% productDAO pDAO=new productDAO();
                List<product> prodList=pDAO.doRetrieveAll();
                for(product p:prodList){

                %>
                <tr>
                    <td> <%=p.getId()%> </td>
                    <td> <%=p.getDesc()%></td>
                    <td> <%=p.getS().getName()%></td>
                    <td class="warning"><%=p.getPrice()%></td>
                    <td> <img class="maglia" src="<%=p.getImage()%>" width="35px!important"> </td>
                    <td>
                    <button id="editProd" name="editProd" value="<%=p.getId()%>">Modifica</button>
                    <form action="editProd">
                    <button id="removeProd" name="removeProd" value="<%=p.getId()%>">Rimuovi</button>
                    </form>
                    </td>
                </tr>

                <div id="myModal" class="modal">


                    <div class="modal-content">
                        <span class="close">&times;</span>
                        <form action="editProd">
                            <input type="hidden" name="idProd" value="<%=p.getId()%>"><br>
                            <label>Descrizione</label>
                        <input type="text" id="newDescProd" name="newDescProd" autocomplete="<%=p.getDesc()%>">
                            <br>
                            <label>Taglia</label>
                        <input type="text" id="newSizeProd" name="newSizeProd" autocomplete="<%=p.getS().getName()%>">
                            <br>
                            <label>Prezzo</label>
                        <input type="text" id="newPriceProd" name="newPriceProd" autocomplete="<%=p.getPrice()%>">
                            <label>Immagine</label>
                            <input type="text" id="newImageProd" name="newImageProd" autocomplete="<%=p.getImage()%>">
                            <input type="submit" name="edit" id="edit" value="Applica">
                        </form>
                    </div>

                </div>

                <%}%>

                <button id="addProd" name="addProd" >Aggiungi nuovo prodotto</button>

                <div id="modalAddProd" class="modal">
                    <div class="modal-content">
                        <span class="close">&times;</span>
                        <form action="addProd">
                            <label>Descrizione</label>
                            <input type="text" name="addDescProd" id="addDesc">
                            <br>
                            <label>Taglia</label>
                            <input type="text" name="addSizeProd" id="addSize">
                            <br>
                            <label>Prezzo</label>
                            <input type="number" name="addPriceProd" id="addPrice">
                            <label>Immagine</label>
                            <input type="text" name="addImageProd" id="addImage">
                            <input type="submit" name="add" value="Aggiungi" onclick="validateForm()">
                        </form>
                    </div>
                </div>


                </tbody>
            </table>
        </div>

    </main>

</div>
</div>


<script>
    var modal = document.getElementById("myModal");
    var modal2 = document.getElementById("modalAddProd");

    var btn = document.getElementById("editProd");
    var btn2 = document.getElementById("addProd");

    var span = document.getElementsByClassName("close")[0];

    btn.onclick = function() {
        modal.style.display = "block";
    }

    btn2.onclick = function() {
        modal2.style.display = "block";
    }

    span.onclick = function() {
        modal.style.display = "none";
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }


    function validateForm() {
        console.log("sono entrato nella funzione validateForm");
        let desc = document.getElementById("addDesc").value;
        console.log(desc);
        let size = document.getElementById("addSize").value;
        console.log(size);
        let price = document.getElementById("addPrice").value;
        console.log(price);
        let image = document.getElementById("addImage").value;
        console.log(image);
        if (desc == "") {
            alert("Devi inserire la descrizione");
        }
        if (size == "") {
            alert("Devi inserire la taglia");
        }
        if (price == "") {
            alert("Devi inserire il prezzo");
        }
        if (image == "") {
            alert("Devi inserire l'immagine");
        }
    }
</script>
</body>
</html>
