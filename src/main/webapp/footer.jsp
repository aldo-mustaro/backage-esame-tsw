
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>footer</title>
    <style>

        body{
            display:flex;
            flex-direction:column;
        }

        .footer{
            height: 219px;
            background: #000;
            color: #8a8a8a;
            font-size: 14px;
            padding: 12px 0 20px;
            margin-top:auto;
        }

        .footer p{
            color: #8a8a8a;
        }

        .footer h3{
            color: #fff;
            margin-bottom: 20px;
        }

        .footer-col-1, .footer-col-2, .footer-col-3, .footer-col-4{
            min-width: 250px;
            margin-bottom: 20px;

        }

        .footer-col-1{
            flex-basis: 30%;
        }

        .footer-col-2{
            flex:1;
            text-align: center;
        }

        .footer-col-2 img{
            width: 180px;
            margin-bottom: 20px;
            background-color:#ffffff;
        }

        .footer-col-3, .footer-col-4{
            flex-basis: 12%;
            text-align: center;
        }

        ul{
            list-style-type: none;
        }

        .app-logo{
            margin-top:20px;
        }
        .app-logo img{
            width:140px;
        }

        .footer hr{
            border: none;
            background: #b5b5b5;
            height: 1px;
            margin: 20px 0;
        }

        .copyright{
            text-align: center;
        }


    </style>
</head>
<body>


<div class="footer">
    <div class="container">
        <div class="row">
            <div class="footer-col-1">
                <h3>Download Our App</h3>
                <p> Download App for Android and ios mobile phone</p>
                <div class="app-logo">
                    <a href="https://play.google.com">
                        <img src="immagini/play-store.png"> </a>
                    <img stc="immagini/ios.png">
                </div>
            </div>
            <div class="footer-col-2">
                <img src="immagini/logo2.png">
            </div>
            <div class="footer-col-3">
                <h3>Area Riservata</h3>
                <a href="loginAdmin.jsp">Login Admin</a>
            </div>
            <div class="footer-col-4">
                <h3>Seguici su tutti i social</h3>
                <p> </p>
            </div>
        </div>
        <hr>
        <p class="copyright"> Copyright 2024 - Aldo Mustaro</p>
    </div>
</div>







</body>
</html>
