<%@ page contentType="text/html; text/css; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="css/loginRegistrazione.css" />
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

</head>


<body>

<%
    String registered= (String) request.getAttribute("registeredEmail");
    if(registered.equals("true")){

%>

    <input type="hidden" id="registrato" value="true">

<% } %>

<div class="login-box">
    <h2>Registrati</h2>
    <form action="registrazione">
        <div class="user-box">
            <input type="text" id="fullname" name="fullname">
            <label>Fullname</label>
        </div>

        <div class="user-box">
            <input type="text" id="username" name="username">
            <label>Username</label>
        </div>


        <div class="user-box">
            <input type="email" id="email" name="email">
            <label>Email</label>
        </div>

        <div class="user-box">
            <input type="text" id="address" name="address">
            <label>Indirizzo</label>
        </div>


        <div class="user-box">
            <input type="password" id="password" name="psw" >
            <label>Password</label>
        </div>

        <div class="user-box">
            <input type="password" id="pswrepeat" name="pswrepeat" >
            <label>Ripeti Password</label>
        </div>


        <a href="#">
            <span></span>
            <span></span>
            <span></span>
            <span></span>

            <input type="hidden" value="false" id="verificato" name="verificato">
            <button id="verifica" onclick="formValidation()" name="registrazione">Registrati</button>
        </a>
    </form>


        <form action="homepage">
            <input type="submit" value="Hai già un account? Effettua il Login" name="login" id="login">
            <input type="submit" value="Home" name="index" id="home">
            </form>

</div>


<script>

    var checkRegistrato=document.getElementById("registrato");
    if(checkRegistrato){
        alert("l'email inserita è già registrata");
    }

    function formValidation(){
        var email=document.getElementById("email");
        var isOk=formValidationQuery(email);
        var verificato=document.getElementById("verificato");
        if (isOk){
            alert("dati corretti");
            verificato.value="true";
        }
        else
            verificato.value="false";
    }

    function formValidationQuery(email)
    {
        var fullname = document.getElementById("fullname");
        var username = document.getElementById("username");
        var address = document.getElementById("address");
        var password = document.getElementById("password");
        var pswRepeat = document.getElementById("pswrepeat");

        console.log("sono entrato nella funzione");
      if(fullname_validation(username,5,12))
    {
        if(passid_validation(password,7,12))
        {
            if(allLetter(fullname))
            {
                if(alphanumeric(address))
                {

                            if(ValidateEmail(email))
                            {
                                return true;
                            }
                }
            }
        }
    }
        return false;

    } function fullname_validation(uid,mx,my)
    {
        var uid_len = uid.value.length;
        if (uid_len == 0 || uid_len >= my || uid_len < mx)
        {
            alert("Username non può essere vuoto e la sua lunghezza deve essere tra "+mx+" e "+my+" caratteri");
            uid.focus();
            return false;
        }
        return true;
    }

    function passid_validation(password,mx,my)
    {
        console.log("password")
        var password_lenght =password.value.length;
        if (password_lenght == 0 ||password_lenght >= my || password_lenght < mx)
        {
            alert(" La password deve essere compresa tra "+mx+" e "+my+" caratteri");
            password_lenght.focus();
            return false;
        }
        return true;
    }


    function allLetter(uname)
    {
        var letters = /^[A-Za-z_ ]+$/;
        if(uname.value.match(letters))
        {
            return true;
        }
        else
        {
            alert('fullname must have alphabet characters only');
            uname.focus();
            return false;
        }
    }


    function alphanumeric(uadd)
    {
        var letters = /^[0-9a-zA-Z_ ]+$/;
        if(uadd.value.match(letters))
        {
            return true;
        }
        else
        {
            alert('User address must have alphanumeric characters only');
            uadd.focus();
            return false;
        }
    }




    function ValidateEmail(uemail)
    {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(uemail.value.match(mailformat))
        {
            return true;
        }
        else
        {
            alert("You have entered an invalid email address!");
            uemail.focus();
            return false;
        }
    }




    function checkEmail(email){
        console.log("sono appena entrato in checkemail");
        $.get('checkRegistrazione', {
                type : "GET",
                "email" : email,
            },
            function(responseText) {
                console.log("responseText della chiamata Ajax " + responseText.isRegistered);
                if(responseText.isRegistered) {
                    alert("Questa email è già registrata");
                    return true;
                }

            }
        )
    return false;};



</script>

</body>
</html>

