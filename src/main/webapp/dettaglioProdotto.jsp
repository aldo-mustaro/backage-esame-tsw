<%@ page import="Model.product" %>
<%@ page import="Model.size" %>
<%@ page import="java.util.List" %>
<%@ page import="DAO.productDAO" %>
<%@ page import="Model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Articolo</title>
    <script src="//code.jquery.com/jquery.min.js"></script>
    <link rel="stylesheet" href="css/homepage.css" type="text/css" />
</head>
<body>

        <div id="nav-placeholder">

        </div>



        <form action="articoli">

        <div class="small-container">
            <div class="row">
                <div class="col-2">
                    <%
                        User u= (User) session.getAttribute("utente");
                        int available= (int) request.getAttribute("qnty");
                        int n=0;
                        product p= (product) request.getAttribute("prod");
                        List<size> listaSize= (List<size>) request.getAttribute("listSize");
                    %>



                    <img src="<%=p.getImage()%>" width="100%" id="imgProd">
                </div>


                <div class="col-2">
                    <h1 ><%=p.getDesc()%></h1>
                    <h4 id="priceProd" value="<%=p.getPrice()%>">€<%=p.getPrice()%></h4>
                    <input type="hidden" id="descProd" name="descProd" value="<%=p.getDesc()%>">

                <% if (available==1) { %>

                    <h1> PRODOTTO NON DISPONIBILE </h1>

                    <%} else{ %>



                    <select id="selectSize" >
                        <option selected>Scegli Taglia</option>
                            <%  for (size s:listaSize){ %>
                        <option value="<%=s.getName()%>" id="sS"> <%=s.getName()%> </option>
                                    <%}%>
                    </select>



                    <input style="display:none" type="number" id="qtny" name="qnty" value="1" min="0" max="<%=n-1%>">

                    <br>



                    <% if(u!=null) { %>
                            <button class="addToCart" id="add" name="add" value="<%=p.getId()%>"> AGGIUNGI AL CARRELLO
                            </button>
                    <%} else { %>
                            <button class="addToCart" id="addNotLogged" name="addNotLogged" value="<%=p.getId()%>"> AGGIUNGI AL CARRELLO
                            </button>
                        <%}%>

                            <button class="addToCart" id="back" name="back" value="back"> Torna ai prodotti
                            </button>

                    <%}%>


                </div>
            </div>
        </div>
        </form>











        <div id="footer-placeholder">

        </div>

        <script>
            $(function(){
                $("#nav-placeholder").load("navbar.jsp");
            });

            $(function(){
                $("#footer-placeholder").load("footer.jsp");
            });



           /* document.querySelector('#addNotLogged').addEventListener('click',()=>{
                let cartItem= {
                    desc: $('#descProd').val(),
                    price: $('#priceProd').val(),
                    quantity: $('#qtny').val(),
                    image: $('#imgProd').src(),
                    size:$('#selectSize').val()
                }
                console.log("116");
                localStorage.setItem("cart",JSON.stringify(cartItem));
                console.log("123");
            });*/


            document.querySelector('#selectSize').addEventListener('change',()=>{
                console.log("107");
                $(function getQuantity() {
                    console.log("109");
                    var size = $('#selectSize').val();
                    console.log("size="+size);
                    var prod = $('#descProd').val();
                    $.get('availableSize', {
                        type : "GET",
                        "selectSize" : size,
                        "descProd" : prod
                    }, function(responseText) {
                        if(document.getElementById("qtny").hasAttribute("style")){
                            document.getElementById("qtny").removeAttribute("style")
                        }
                        console.log("116");
                        document.getElementById("qtny").max=responseText.quantity;
                        console.log("116");
                    });
                });
            });







        </script>
</body>
</html>
