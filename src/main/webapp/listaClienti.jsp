<%@ page import="DAO.productDAO" %>
<%@ page import="Model.product" %>
<%@ page import="java.util.List" %>
<%@ page import="DAO.userDAO" %>
<%@ page import="Model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Sharp" rel="stylesheet">
    <link rel="stylesheet" href="css/adminView.css">
    <title>Title</title>
</head>
<body>
<div class="container">
    <aside>
        <div class="top">
            <div class="logo">
                <a href="index.jsp"></a>
                <img src="immagini/logo2.png">
                </a>
            </div>
            <div class="close" id="close-btn">
                <span class="material-icon-shard">close</span>
            </div>
        </div>

        <div class="sidebar">
            <a href="homepageAdmin.jsp">
                <span class="material-icons-sharp">grid_view</span>
                <h3>Dashboard</h3>
            </a>
            <a href="listaClienti.jsp">
                <span class="material-icons-sharp">person_outline</span>
                <h3>Customers</h3>
            </a>
            <a href="listaOrdini.jsp">
                <span class="material-icons-sharp">receipt_long</span>
                <h3>Orders</h3>
            </a>
            <a href="listaProdotti.jsp">
                <span class="material-icons-sharp">inventory</span>
                <h3>Products</h3>
            </a>


            <a href="#">
                <span class="material-icons-sharp">logout</span>
                <h3>Log Out</h3>
            </a>

        </div>
    </aside>



    <main>
        <div class="recent-order">
            <h2>LISTA PRODOTTI</h2>
            <table>
                <thead>
                <tr>
                    <th> Id utente </th>
                    <th> Username </th>
                    <th> Fullname </th>
                    <th> Indirizzo </th>
                    <th> Email </th>
                    <th> Password </th>
                </tr>
                </thead>
                <tbody>

                <% userDAO uDAO=new userDAO();
                    List<User> userList=uDAO.doRetrieveAll();
                    for(User u:userList){

                %>
                <tr>
                    <td> <%=u.getId()%> </td>
                    <td> <%=u.getUsername()%></td>
                    <td> <%=u.getFullname()%></td>
                    <td> <%=u.getAddress()%></td>
                    <td> <%=u.getEmail()%></td>
                    <td> <%=u.getPassword()%></td>
                </tr>

                <%}%>

                </tbody>
            </table>
        </div>

    </main>

</div>
</div>

</body>
</html>
