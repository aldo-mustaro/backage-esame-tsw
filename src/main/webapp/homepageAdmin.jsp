
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Sharp" rel="stylesheet">
    <link rel="stylesheet" href="css/adminView.css">
    <title>Title</title>
</head>
<body>
    <div class="container">
        <aside>
            <div class="top">
                <div class="logo">
                    <a href="index.jsp"></a>
                    <img src="immagini/logo2.png">
                    </a>
                </div>
                <div class="close" id="close-btn">
                    <span class="material-icon-shard">close</span>
                </div>
            </div>

            <div class="sidebar">
                <a href="homepageAdmin.jsp">
                    <span class="material-icons-sharp">grid_view</span>
                    <h3>Dashboard</h3>
                </a>
                <a href="listaClienti.jsp">
                    <span class="material-icons-sharp">person_outline</span>
                    <h3>Customers</h3>
                </a>
                <a href="listaOrdini.jsp">
                    <span class="material-icons-sharp">receipt_long</span>
                    <h3>Orders</h3>
                </a>
                <a href="listaProdotti.jsp">
                    <span class="material-icons-sharp">inventory</span>
                    <h3>Products</h3>
                </a>


                <a href="index.jsp">
                    <span class="material-icons-sharp">logout</span>
                    <h3>Log Out</h3>
                </a>

            </div>
        </aside>



        <main>
            <div class="recent-order">
                <h2>Benvenuto Admin</h2>

            </div>

        </main>

        </div>
    </div>

</body>
</html>
