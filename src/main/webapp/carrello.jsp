<%@ page import="Model.User" %>
<%@ page import="Model.cart" %>
<%@ page import="java.util.List" %>
<%@ page import="DAO.productDAO" %>
<%@ page import="Model.product" %><%--
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>CARRELLO</title>
    <script src="//code.jquery.com/jquery.min.js"></script>
    <link rel="stylesheet" href="css/carrello.css" type="text/css" />
</head>
<body>

<div id="nav-placeholder">

</div>

    <table>

        <tr>
            <th>Product</th>
            <th>Quantity</th>
            <th>Subtotale</th>
        </tr>
            <div class="small-container cart-page">

        <% User u= (User) session.getAttribute("utente");
            productDAO pDAO=new productDAO();
            int total=0;
           if(u!=null){
               List<cart> c = (List<cart>) session.getAttribute("carrello");
           %>

                <input type="hidden" id="user" name="user" value="<%=u.getFullname()%>">


        <%
            for (cart carrello : c) {
                product p=pDAO.doRetrieveById(carrello.getIdprod());
        %>



                <tr>
                    <td>
                        <input type="hidden" id="descProd" name="descProd" value="<%=p.getDesc()%>">
                        <input type="hidden" id="sizeProd" name="sizeProd" value="<%=p.getS().getId()%>">
                        <div class="cart-info">
                            <img src="<%=p.getImage()%>">
                            <div>
                                <p> <%=p.getDesc()%> </p>
                                <p> <%=p.getS().getName()%> </p>
                                <small> <%=p.getPrice()%> </small>
                                <br>
                                    <form action="carrello">
                                        <button value=<%=p.getId()%> name="remove" id="remove"> Rimuovi </button>
                                        <!---<button value=p.getIdJava name="add" id="add"> + </button>
                                        <button value= p.getIdJava name="sub" id="sub"> - </button> ---->
                                    </form>
                                </div>
                        </div>
                        </td>
                    <td><%=carrello.getQuantity()%></td>
                    <td><%=p.getPrice()*carrello.getQuantity()%></td>
                </tr>

                <%total=total+p.getPrice()*carrello.getQuantity();%>




        <% } %>


                <div class="total-price">
                    <table>
                        <tr>
                            <td>Subtotal</td>
                            <td><%=total%></td>
                        </tr>
                        <tr>
                            <td>Spedizione</td>
                            <td>€5</td>
                        </tr>
                        <tr>
                            <td>Totale</td>
                            <td><%=total + 5%></td>
                        </tr>
                    </table>
                </div>
                <li><a href="checkout.jsp">Checkout</a></li>
            </div>


        <%} else { %>



                <tr>
                    <td>
                        <div class="cart-info">
                            <img id="imgProd" src="">
                            <div>
                                <p id="descrizione"></p>
                                <p id="taglia"></p>
                                <small id="prezzo"></small>
                                <br>
                                <form action="carrello">
                                    <button value="" name="remove2"> Rimuovi </button>
                                    <!---<button value=p.getIdJava name="add" id="add"> + </button>
                                    <button value= p.getIdJava name="sub" id="sub"> - </button> ---->
                                </form>
                            </div>
                        </div>
                    </td>
                    <td id="quantity"></td>
                    <td>totale prezzo articolo*quantità</td>
                </tr>




        <form action="homepage">
            <input type="submit" value="Effettua il Login" name="login" id="login">
        </form>



        <% } %>

        <form action="account">
            <input type="submit" value="Effettua il LogOut" name="logOut" id="logOut">
        </form>

        <form action="carrello" onsubmit="window.location.reload()">
            <input type="submit" value="Svuota carrello" name="svuota" id="svuota">
        </form>

        <form action="homepage">
            <input type="submit" value="Home" name="index" id="index">
        </form>

    </table>



<div id="footer-placeholder">

</div>




<script>
    $(function(){
        $("#nav-placeholder").load("navbar.jsp");
    });

    $(function(){
        $("#footer-placeholder").load("footer.jsp");
    });


   /* let cart={};

    let tr = document.createElement('tr');

    let title_td = document.createElement('td');


    $( document ).ready(function() {
        console.log("177");
    if (localStorage.getItem("cart")) {
        console.log("179");
        cart = JSON.parse(localStorage.getItem("cart"));
        document.getElementById("imgProd").src=cart.image;
        document.getElementById("descrizione").innerHTML=cart.desc;
        document.getElementById("taglia").innerHTML=cart.size;
        document.getElementById("prezzo").innerHTML=cart.price;
        document.getElementById("quantity").innerHTML=cart.quantity;
        console.log("186");
    }})

    function svuotaLocalStorage(){
        localStorage.clear();
        localS();
    }*/


    var user = $('#user').val();

    function localS() {
        if(user!=null) {
            console.log("144");
            var size = $('#sizeProd').val();
            var prod = $('#descProd').val();
            var qtny = $('#quantity').innerHTML();
            $.get('availableProduct', {
                type: "GET",
                "sizeProd": size,
                "descProd": prod
            }, function (responseText) {
                if (!responseText.isAvailable)
                    document.getElementById("quantity").innerHTML = "PRODOTTO NON DISPONIBILE";
                else if (qtny > responseText.quantity)
                    document.getElementById("quantity").innerHTML = responseText.quantity;
            });
        } }


    $( document ).ready(localS());




</script>



</body>
</html>
