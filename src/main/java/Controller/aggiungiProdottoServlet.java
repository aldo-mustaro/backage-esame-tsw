package Controller;

import java.io.*;
import java.util.List;

import DAO.cartDAO;
import DAO.productDAO;
import DAO.sizeDAO;
import Model.User;
import Model.cart;
import Model.product;
import Model.size;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "addProd", value = "/addProd")
public class aggiungiProdottoServlet extends HttpServlet {
    private String message;
    public void init() {

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String address="listaProdotti.jsp";
        productDAO pDAO=new productDAO();
        String add=request.getParameter("add");

        if(add!=null){
            String newDesc=request.getParameter("addDescProd");
            String newSize=request.getParameter("addSizeProd");
            String image=request.getParameter("addImageProd");
            sizeDAO sDAO=new sizeDAO();
            size s=sDAO.doRetrieveByName(newSize);
            if(((newDesc.equals(""))||(newSize.equals(""))||(image.equals("")))){

            }
            else {
                int newPrice = Integer.parseInt(request.getParameter("addPriceProd"));
                product p = new product();
                p.setDesc(newDesc);
                p.setS(s);
                p.setImage(image);
                p.setPrice(newPrice);
                pDAO.doSave(p);
            }

        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(address);
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    public void destroy() {
    }
}