package Controller;

import java.io.*;
import java.util.List;

import DAO.cartDAO;
import DAO.productDAO;
import DAO.sizeDAO;
import DAO.userDAO;
import Model.User;
import Model.cart;
import Model.product;
import Model.size;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "checkRegistrazione", value = "/checkRegistrazione")
public class checkRegistrazioneServlet extends HttpServlet {
    private String message;


    public void init() {
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {


        String email=request.getParameter("email");
        userDAO uDAO= new userDAO();
        User u=uDAO.checkEmail(email);
        boolean isRegistered=false;
        if(u!=null){
            isRegistered=true;
        }


       /* request.setAttribute("sizeQuantity",n);

        String address="dettaglioProdotto.jsp";

        RequestDispatcher dispatcher = request.getRequestDispatcher(address);
        dispatcher.forward(request, response);*/

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String json = "{\"isRegistered\" : " + isRegistered + "}";

        response.getWriter().write(json);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    public void destroy() {
    }
}