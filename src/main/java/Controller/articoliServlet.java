package Controller;

import java.io.*;
import java.util.List;

import DAO.cartDAO;
import DAO.productDAO;
import Model.User;
import Model.cart;
import Model.product;
import Model.size;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "articoli", value = "/articoli")
public class articoliServlet extends HttpServlet {
    private String message;
    //Qui vanno istruzioni per richiamare il database (DAO), sessioni per mantenere lo stato, passare lo stato alla jsp di articoli
    public void init() {
        message = "Articoli";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String address="articoli.jsp";
        String add=request.getParameter("add");
        productDAO pDAO=new productDAO();
        HttpSession session = request.getSession(false);
        User u= (User) session.getAttribute("utente");
        productDAO prodottoDAO = new productDAO();
        List<product> prodottoList = prodottoDAO.doRetrieveAll();
        List<product> listDesc = prodottoDAO.doRetrieveDesc();
        request.setAttribute("prodottoList", listDesc);
        String goTo=request.getParameter("goTo");
        String back=request.getParameter("back");
        String notLogged=request.getParameter("addNotLogged");



        int num=0;
        request.setAttribute("sizeQuantity",num);

        if(notLogged!=null){
            address="login.jsp";
        }

        if(back!=null){
            address="articoli.jsp";
        }



        if(goTo!=null){
           for (product prod : prodottoList){
                if (prod.getDesc().equals(goTo)) {
                    List<size> listSize=prodottoDAO.doRetrieveSize(prod.getDesc());
                    int n=prodottoDAO.doRetrieveCount(prod.getDesc()); //devp fare la stessa cosa ma tenendo conto della taglia
                    request.setAttribute("qnty",n);
                    request.setAttribute("prod",prod);
                    request.setAttribute("listSize",listSize);
            }}
           address="dettaglioProdotto.jsp";
        }

       /* if(goTo!=null){
            for (product prod : prodottoList){
                if (prod.getDesc().equals(goTo)) {
                    List<size> listSize=prodottoDAO.doRetrieveSize(prod.getDesc());
                    int[] quantitySize = new int[listSize.size()];
                    for(size s: listSize){
                        int n=prodottoDAO.doRetrieveCountSize(prod.getDesc(),s.getId());
                    }
                    int n5=prodottoDAO.doRetrieveCountSize(prod.getDesc(),5);//devp fare la stessa cosa ma tenendo conto della taglia

                    request.setAttribute("qnty",n);
                    request.setAttribute("prod",prod);
                    request.setAttribute("listSize",listSize);
                }}
            address="dettaglioProdotto.jsp";
        }*/



        if (add != null) {         //aggiungi al carrello dalla homapage
            int codice = Integer.parseInt(request.getParameter("add"));
            String quantita=request.getParameter("qnty");
            if(u!=null) {

                List<cart> car= (List<cart>) session.getAttribute("carrello");

                product prodotto = pDAO.doRetrieveById(codice);
                cart carrello = new cart(codice, Integer.parseInt(quantita), u.getId());
                cartDAO carrelloDAO = new cartDAO();
                carrello.setId(carrelloDAO.doSave(carrello));
                car.add(carrello);



                synchronized (session) {
                    session.setAttribute("carrello", car);
                }


            } else{
                address="login.jsp";
            }
        }


        RequestDispatcher dispatcher = request.getRequestDispatcher(address);
        dispatcher.forward(request, response);
    }

    public void destroy() {
    }
}