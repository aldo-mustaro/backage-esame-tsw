package Controller;

import java.io.*;
import java.util.List;

import DAO.cartDAO;
import DAO.productDAO;
import Model.User;
import Model.cart;
import Model.product;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "homepage", value = "/homepage")
public class homepageServlet extends HttpServlet {

    public void init() {

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String articoli = request.getParameter("articoli");
        String account = request.getParameter("account");
        String carrello = request.getParameter("carrello");
        String login = request.getParameter("login");
        String registrazione = request.getParameter("registrazione");
        String index = request.getParameter("index");
        HttpSession session = request.getSession(false);
        String address=null;


        if(index!=null)
            address="index.jsp";
        if(articoli!=null){
            productDAO prodottoDAO = new productDAO();
            List<product> prodottoList = prodottoDAO.doRetrieveDesc();
            request.setAttribute("prodottoList", prodottoList);
            address="articoli.jsp";}

        if(account!=null)
            address="account.jsp";

        if(carrello!=null) {
            cartDAO cDAO = new cartDAO();
            User u= (User) session.getAttribute("utente");
            if(u!=null){
                List<cart> car = cDAO.doRetrieveByUserId(u.getId());
                synchronized (session){
                    session.setAttribute("carrello",car);
                }
                address="carrello.jsp";
            }
            else{
                address="login.jsp";}}

        if(login!=null)
            address="login.jsp";
        if(registrazione!=null){
            String isRegistered="false";
            request.setAttribute("registeredEmail",isRegistered);
            address="registrazione.jsp";}

        RequestDispatcher dispatcher = request.getRequestDispatcher(address);
        dispatcher.forward(request, response);
    }

    public void destroy() {
    }
}
