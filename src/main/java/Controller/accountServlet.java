package Controller;

import java.util.ArrayList;
import java.util.List;

import DAO.cartDAO;
import DAO.orderDAO;
import DAO.orderDetailsDAO;
import DAO.userDAO;
import Model.User;
import Model.order;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import java.util.List;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "account", value = "/account")
public class accountServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String logOut = request.getParameter("logOut");
        String ordini= request.getParameter("ordini");
        String address = null;
        HttpSession session = request.getSession(true);


        if(ordini!=null){
            User u= (User) session.getAttribute("utente");
            orderDAO oDAO=new orderDAO();
            cartDAO cDAO=new cartDAO();
            orderDetailsDAO oDDAO=new orderDetailsDAO();
            List<order> listaOrdini=oDAO.doRetrieveByUserId(u.getId());
            address="riepilogoOrdini.jsp";
            synchronized(session) {
                session.setAttribute("listaOrdini", listaOrdini);
            }

        }


        if (logOut != null) {
            session.invalidate();
            address = "index.jsp";
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher(address);
        dispatcher.forward(request, response);
    }

}

