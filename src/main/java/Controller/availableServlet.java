package Controller;

import java.io.*;
import java.util.List;

import DAO.cartDAO;
import DAO.productDAO;
import DAO.sizeDAO;
import Model.User;
import Model.cart;
import Model.product;
import Model.size;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "availableSize", value = "/availableSize")
public class availableServlet extends HttpServlet {
    private String message;


    public void init() {
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        productDAO prodottoDAO=new productDAO();
        sizeDAO sDAO=new sizeDAO();

        List<product> prodottoList = prodottoDAO.doRetrieveAll();
        String selected=request.getParameter("selectSize");
        String desc=request.getParameter("descProd");
        int n=-1;

        for (product prod : prodottoList){
            if((prod.getS().getId()==sDAO.doRetrieveByName(selected).getId()) && (prod.getDesc().equals(desc)))
                n=prodottoDAO.doRetrieveCountSize(prod.getDesc(),sDAO.doRetrieveByName(selected).getId());
        }

       /* request.setAttribute("sizeQuantity",n);

        String address="dettaglioProdotto.jsp";

        RequestDispatcher dispatcher = request.getRequestDispatcher(address);
        dispatcher.forward(request, response);*/

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String json = "{\"quantity\" : " + n + "}";

        response.getWriter().write(json);


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    public void destroy() {
    }
}