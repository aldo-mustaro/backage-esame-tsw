package Controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import DAO.*;
import Model.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import java.util.List;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "checkout", value = "/checkout")
public class checkoutServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String address ="riepilogoOrdne.jsp";
        HttpSession session = request.getSession(false);
        List<cart> carrelloList = (List<cart>) session.getAttribute("carrello");
        User u= (User) session.getAttribute("utente");
        orderDAO oDAO=new orderDAO();
        cartDAO cDAO=new cartDAO();
        productDAO pDAO=new productDAO();
        orderDetailsDAO oDDAO=new orderDetailsDAO();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println(dtf.format(now));
        String indirizzo=request.getParameter("via") +" "+ request.getParameter("cap") + " " + request.getParameter("citta");


        order o=new order(u.getId(),"Creato",dtf.format(now),indirizzo);
        o.setId(oDAO.doSave(o));
        int price;
        String desc;
        orderDetails oD=new orderDetails();
        for (cart carrello : carrelloList) {
            price=cDAO.getPrice(carrello.getIdprod());
            desc=cDAO.getDesc(carrello.getIdprod());
            oD.setIdorders(o.getId());
            oD.setIdprod(carrello.getIdprod());
            oD.setQuantity(carrello.getQuantity());
            pDAO.deleteFromBuying(carrello.getIdprod(),carrello.getQuantity());
            oD.setPrice(price*carrello.getQuantity());
            oD.setDesc(desc);
            oD.setId(oDDAO.doSave(oD));

        }

        List<orderDetails> report =oDDAO.doRetrieveByIdOrder(o.getId());

        request.setAttribute("Ordine",o);
        request.setAttribute("report",report);
        cDAO.deleteByIdUser(u.getId());



        RequestDispatcher dispatcher = request.getRequestDispatcher(address);
        dispatcher.forward(request, response);
    }

}