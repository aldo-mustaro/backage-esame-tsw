package Controller;

import java.util.ArrayList;
import java.util.List;

import DAO.cartDAO;
import DAO.orderDAO;
import DAO.orderDetailsDAO;
import DAO.userDAO;
import Model.User;
import Model.order;
import Model.orderDetails;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import java.util.List;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "ordini", value = "/ordini")
public class ordiniServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int ordine= Integer.parseInt(request.getParameter("ordine"));
        String address = null;
        HttpSession session = request.getSession(true);
        List<order> listaOr= (List<order>) session.getAttribute("listaOrdini");
        orderDetailsDAO oDDAO=new orderDetailsDAO();
        for (order ord : listaOr) {
            if (ord.getId()==ordine){
                List<orderDetails> report =oDDAO.doRetrieveByIdOrder(ord.getId());
                request.setAttribute("Ordine",ord);
                request.setAttribute("report",report);
            }
        }

        address="riepilogoOrdne.jsp";
        RequestDispatcher dispatcher = request.getRequestDispatcher(address);
        dispatcher.forward(request, response);
    }

}

