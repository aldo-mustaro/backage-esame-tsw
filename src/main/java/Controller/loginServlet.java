package Controller;

import java.util.ArrayList;
import java.util.List;

import DAO.adminDAO;
import DAO.cartDAO;
import DAO.userDAO;
import Model.User;
import Model.admin;
import Model.cart;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import java.util.List;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "login", value = "/login")
public class loginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String address=null;
        String accedi = request.getParameter("accedi");
        String registrazione = request.getParameter("registrati");
        String home = request.getParameter("home");
        String admin=request.getParameter("accediAdmin");
        HttpSession session = request.getSession(true);
        boolean validazione = true;

        if(registrazione != null){
            address = "registrazione.jsp";
        }

        if(home != null){
            address = "index.jsp";
        }

        if(accedi != null){
            User utente = new User();
            userDAO utenteDAO = new userDAO();

            String email = request.getParameter("email");
            String password = request.getParameter("psw");

                    utente = utenteDAO.doRetrieveByCredenziali(email, password);


                if(utente!=null){
                        address = "index.jsp";
                        cartDAO carDAO=new cartDAO();
                        List<cart> car=carDAO.doRetrieveByUserId(utente.getId());

                    synchronized (session){
                        session.setAttribute("utente", utente);
                        session.setAttribute("carrello",car);
                    }
                }
                else {
                    address =  "login.jsp";
                }


        }

        if(admin!=null){
            admin a=new admin();
            adminDAO aDAO=new adminDAO();

            String username = request.getParameter("username");
            String password = request.getParameter("psw");

            a = aDAO.doRetrieveByCredenziali(username, password);

            if(a!=null){
                address = "homepageAdmin.jsp";
            }
            else {
                address =  "error.jsp";
            }


        }

        if(!validazione)
            address = "error.jsp";

         RequestDispatcher dispatcher = request.getRequestDispatcher(address);
        dispatcher.forward(request, response);

    }
}
