package Controller;

import java.io.*;
import java.util.List;

import DAO.cartDAO;
import DAO.productDAO;
import DAO.sizeDAO;
import Model.User;
import Model.cart;
import Model.product;
import Model.size;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "editProd", value = "/editProd")
public class modificaProdottoServlet extends HttpServlet {
    private String message;
    //Qui vanno istruzioni per richiamare il database (DAO), sessioni per mantenere lo stato, passare lo stato alla jsp di articoli
    public void init() {
        message = "Articoli";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String address="listaProdotti.jsp";
        String remove=request.getParameter("removeProd");
        productDAO pDAO=new productDAO();
        String edit=request.getParameter("edit");


        if(remove!=null){
            int r= Integer.parseInt(remove);
            pDAO.deleteById(r);
        }

        if(edit!=null){
            String newDesc=request.getParameter("newDescProd");
            String newSize=request.getParameter("newSizeProd");
            int newPrice= Integer.parseInt(request.getParameter("newPriceProd"));
            int idProd=Integer.parseInt(request.getParameter("idProd"));
            String image=request.getParameter("newImageProd");
            sizeDAO sDAO=new sizeDAO();
            size s=sDAO.doRetrieveByName(newSize);
            pDAO.doEdit(idProd,newDesc,s.getId(),newPrice,image);


        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(address);
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    public void destroy() {
    }
}