package Controller;

import java.io.*;
import java.util.List;

import DAO.cartDAO;
import DAO.productDAO;
import DAO.sizeDAO;
import Model.User;
import Model.cart;
import Model.product;
import Model.size;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "availableProduct", value = "/availableProduct")
public class availableProduct extends HttpServlet {
    private String message;


    public void init() {
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        productDAO prodottoDAO=new productDAO();
        sizeDAO sDAO=new sizeDAO();
        String desc=request.getParameter("descProd");
        int s= Integer.parseInt(request.getParameter("sizeProd"));
        boolean isAvailable=false;

        int n=prodottoDAO.doRetrieveCountSize(desc,s);
        if(n>1)
            isAvailable=true;


        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String json = "{\"isAvailable\" : " + isAvailable + ","+"\n \"quantity\" : " + n + "}";

        response.getWriter().write(json);


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    public void destroy() {
    }
}