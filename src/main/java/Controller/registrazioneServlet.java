package Controller;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import DAO.cartDAO;
import DAO.productDAO;
import DAO.userDAO;
import Model.User;
import Model.cart;
import Model.product;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "registrazione", value = "/registrazione")
public class registrazioneServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String registrazione = request.getParameter("registrazione");
        String home = request.getParameter("home");
        String address = null;
        String verificato=request.getParameter("verificato");
        HttpSession session = request.getSession(true);

        if(login != null){
            address = "login.jsp";
        }

        if(home != null){
            address = "index.jsp";
        }

        boolean ris = true;


        if(verificato.equals("true")) {
            userDAO uDAO = new userDAO();
            User u = uDAO.checkEmail(request.getParameter("email"));
            if (u != null) {
                address = "registrazione.jsp";
                request.setAttribute("registeredEmail", "true");
            } else {

                if (registrazione != null) {
                    User utente = new User();
                    userDAO utenteDAO = new userDAO();
                    String password = request.getParameter("psw");
                    String passwordRipetuta = request.getParameter("pswrepeat");
                    utente.setPassword(password);
                    utente.setAddress(request.getParameter("address"));
                    utente.setEmail(request.getParameter("email"));
                    utente.setFullname(request.getParameter("fullname"));
                    utente.setUsername(request.getParameter("username"));

                    if (ris) {
                        request.setAttribute("utente", utente);
                        utenteDAO.doSave(utente);


                        ArrayList<cart> car = new ArrayList<>();


                        synchronized (session) {
                            session.setAttribute("utente", utente);
                            session.setAttribute("carrello", car);
                        }

                        address = "index.jsp";
                    }

                }
            }
        }

        else{
            request.setAttribute("registeredEmail", "false");
            address="registrazione.jsp";
        }


        if(!ris){
            address = "error.jsp";
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher(address);
        dispatcher.forward(request, response);
    }
}