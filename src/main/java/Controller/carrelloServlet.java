package Controller;

import java.util.ArrayList;
import java.util.List;

import DAO.cartDAO;
import DAO.userDAO;
import Model.User;
import Model.cart;
import Model.product;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import java.util.List;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "carrello", value = "/carrello")
public class carrelloServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);



    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String address = null;
        String svuota=request.getParameter("svuota");
        String cancella=request.getParameter("remove");
        String add=request.getParameter("add");
        String sub=request.getParameter("sub");
        String checkout=request.getParameter("checkout");
        HttpSession session = request.getSession(false);
        List<cart> prodottoList = (List<cart>) session.getAttribute("carrello");

        if(cancella!=null){
            int id= Integer.parseInt(request.getParameter("remove"));
            User u= (User) session.getAttribute("utente");
            cartDAO car=new cartDAO();
            car.deleteItem(id,u.getId());
            List<cart> nuovaLista=car.doRetrieveByUserId(u.getId());
            synchronized (session){
                session.setAttribute("carrello",nuovaLista);
            }
            address="carrello.jsp";
        }



        if(svuota!=null){
            User u= (User) session.getAttribute("utente");
            cartDAO car=new cartDAO();
            car.deleteByIdUser(u.getId());
            address="index.jsp";
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(address);
        dispatcher.forward(request, response);
    }

}