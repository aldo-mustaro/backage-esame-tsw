package DAO;

import Model.User;
import Model.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class adminDAO {


    public admin doRetrieveById(int id) {
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps =
                    con.prepareStatement("SELECT id, username, pass, fullname FROM administrator WHERE id=?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                admin p = new admin();
                p.setId(id);
                p.setUsername(rs.getString(2));
                p.setPassword(rs.getString(3));
                p.setFullname(rs.getString(4));
                return p;
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public admin doRetrieveByCredenziali(String username, String password) {
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps =
                    con.prepareStatement("SELECT id, username, pass, fullname FROM administrator WHERE username=? and pass=?");
            ps.setString(1, username);
            ps.setString(2,password);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                admin p = new admin();
                p.setId(rs.getInt(1));
                p.setUsername(rs.getString(2));
                p.setPassword(rs.getString(3));
                p.setFullname(rs.getString(4));
                return p;
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }



    //carica da un form al database
    public void doSave(admin a) {
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps = con.prepareStatement(
                    "INSERT INTO administrator (username, pass, fullname) VALUES(?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, a.getUsername());
            ps.setString(2, a.getPassword());
            ps.setString(3, a.getFullname());
            if (ps.executeUpdate() != 1) {
                throw new RuntimeException("INSERT error.");
            }
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            int id = rs.getInt(1);
            a.setId(id);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public List<admin> doRetrieveAll(){
        ArrayList<admin> admins=new ArrayList<>();

        Statement st;
        ResultSet rs;
        admin p;

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeQuery("SELECT * FROM administrator Where 1=1");

            while(rs.next()){
                p=new admin();
                p.setId(rs.getInt(1));
                p.setUsername(rs.getString(2));
                p.setPassword(rs.getString(3));
                p.setFullname(rs.getString(4));
                admins.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return admins;
    }


    public void doUpdate(admin c){
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="update administrator set username='"+ c.getUsername() + "',pass='" + c.getPassword() + "',fullname=" + c.getFullname() + "where id=" + c.getId() + ";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    public void delete(admin c){
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="delete * from administrator where id=" + c.getId() + ";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    //METODI CRUD: CREATE READ UPDATE DELETE,

}



