package DAO;

import Model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * A small table of banking customers for testing.
 */

public class userDAO {

    /**
     * Finds the customer with the given ID.
     * Returns null if there is no match.
     */

    public User doRetrieveById(int id) {
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps =
                    con.prepareStatement("SELECT id, fullname, username, email FROM customer WHERE id=?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                User p = new User();
                p.setId(rs.getInt(1));
                p.setFullname(rs.getString(2));
                p.setUsername(rs.getString(3));
                p.setEmail(rs.getString(4));
                return p;
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public User doRetrieveByCredenziali(String email, String password) {
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps =
                    con.prepareStatement("SELECT id, fullname, username, email FROM customer WHERE email=? and pass=?");
            ps.setString(1, email);
            ps.setString(2,password);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                User p = new User();
                p.setId(rs.getInt(1));
                p.setFullname(rs.getString(2));
                p.setUsername(rs.getString(3));
                p.setEmail(rs.getString(4));
                return p;
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public User checkEmail(String email){
        ResultSet rs;
        User u=null;
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="select * from customer where email='"+email +"';";
            rs=st.executeQuery(query);

            if (rs.next()) {
                u=new User();
               u.setId(rs.getInt(1));
               u.setFullname(rs.getString(2));
               u.setUsername(rs.getString(3));
               u.setEmail(rs.getString(4));
               u.setAddress(rs.getString(5));
               u.setPassword(rs.getString(6));
            }


        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
        return u;
    }


//carica da un form al database
    public void doSave(User customer) {
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps = con.prepareStatement(
                    "INSERT INTO customer (fullname, username, email, address, pass) VALUES(?,?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, customer.getFullname());
            ps.setString(2, customer.getUsername());
            ps.setString(3, customer.getEmail());
            ps.setString(4, customer.getAddress());
            ps.setString(5, customer.getPassword());
            if (ps.executeUpdate() != 1) {
                throw new RuntimeException("INSERT error.");
            }
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            int id = rs.getInt(1);
            customer.setId(id);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public List<User> doRetrieveAll(){
        ArrayList<User> users=new ArrayList<>();

        Statement st;
        ResultSet rs;
        User p;

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeQuery("SELECT * FROM customer Where 1=1");

            while(rs.next()){
                p=new User();
                p.setId(rs.getInt(1));
                p.setFullname((rs.getString(2)));
                p.setUsername(rs.getString(3));
                p.setEmail(rs.getString(4));
                p.setAddress(rs.getString(5));
                p.setPassword(rs.getString(6));
                users.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }


    public void doUpdateCustomer(User c){
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="update customer set fullname='"+ c.getFullname() + "',username='" + c.getUsername() + "',email='" + c.getEmail() + "',address='" + c.getAddress() + "',pass=" + c.getPassword() + "where id=" + c.getId() + ";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    public void deleteCustomer(User c){
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="delete * from customer where id=" + c.getId() + ";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }


    /*public void doUpdateById(int id){
        try(Connection con= ConPool.getConnection()){
            PreparedStatement ps=con.prepareStatement("Update customer SET bookmarked= ? WHERE id=?");
            ps.setBoolean(1,pref);
            ps.setInt(2,id);
            if(ps.executeUpdate()!=1)
            {
                throw new RuntimeException("Update error");
            }
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }*/


    /*public List<User> doRetrievePrefs(){
        ArrayList<User> users=new ArrayList<>();

        Statement st;
        ResultSet rs;
        User p;

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeQuery("SELECT * FROM Customer Where bookmarked=1");

            while(rs.next()){
                p=new User();
                p.setId(rs.getInt(1));
                p.setFirstName((rs.getString(2)));
                p.setLastName(rs.getString(3));
                p.setEmail(rs.getString(4));
                users.add(p);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }*/



    //METODI CRUD: CREATE READ UPDATE DELETE,

}


