package DAO;

import Model.orderDetails;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class orderDetailsDAO {


    public orderDetails doRetrieveById(int id) {
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps =
                    con.prepareStatement("SELECT idorder, idprod, quantity, price FROM ordersDetails WHERE id=?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                orderDetails p = new orderDetails();
                p.setId(id);
                p.setIdorders(rs.getInt(2));
                p.setIdprod(rs.getInt(1));
                p.setQuantity(rs.getInt(3));
                p.setPrice(rs.getInt(4));
                return p;
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    //carica da un form al database
    public int doSave(orderDetails ord) {
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps = con.prepareStatement(
                    "INSERT INTO ordersDetails (idorders, idprod, quantity, price, descr) VALUES(?,?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, ord.getIdorders());
            ps.setInt(2, ord.getIdprod());
            ps.setInt(3, ord.getQuantity());
            ps.setInt(4, ord.getPrice());
            ps.setString(5,ord.getDesc());
            if (ps.executeUpdate() != 1) {
                throw new RuntimeException("INSERT error.");
            }
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            int id = rs.getInt(1);
            return id;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public List<orderDetails> doRetrieveAll(){
        ArrayList<orderDetails> orders=new ArrayList<>();

        Statement st;
        ResultSet rs;
        orderDetails p;

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeQuery("SELECT * FROM ordersDetails Where 1=1");

            while(rs.next()){
                p=new orderDetails();
                p.setId(rs.getInt(1));
                p.setIdorders(rs.getInt(3));
                p.setIdprod(rs.getInt(2));
                p.setQuantity(rs.getInt(4));
                p.setPrice(rs.getInt(5));
                orders.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }




    public List<orderDetails> doRetrieveByIdOrder(int idOrder){
        ArrayList<orderDetails> orders=new ArrayList<>();

        Statement st;
        ResultSet rs;
        orderDetails p;

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeQuery("select * from ordersdetails where idorders="+idOrder+";");

            while(rs.next()){
                p=new orderDetails();
                p.setId(rs.getInt(1));
                p.setIdorders(rs.getInt(2));
                p.setIdprod(rs.getInt(3));
                p.setQuantity(rs.getInt(4));
                p.setPrice(rs.getInt(5));
                p.setDesc(rs.getString(6));
                orders.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }


    public void doUpdate(orderDetails c){
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="update ordersDetails set idorder='"+ c.getIdorders() + "',idprod='" + c.getIdprod() + "',quantity='" + c.getQuantity() + "',price=" + c.getPrice() + "where id=" + c.getId() + ";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    public void delete(orderDetails c){
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="delete * from ordersDetails where id=" + c.getId() + ";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    //METODI CRUD: CREATE READ UPDATE DELETE,

}



