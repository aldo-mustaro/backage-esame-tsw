package DAO;

import Model.product;
import Model.size;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class productDAO {


    public product doRetrieveById(int id){
        Statement st;
        ResultSet rs;
        product p=new product();

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeQuery("select * from product where id="+id+";");

            while(rs.next()){
                p=new product();
                p.setId(rs.getInt(1));
                p.setDesc(rs.getString(2));
                p.setImage(rs.getString(5));
                p.setPrice(rs.getInt(3));
                sizeDAO si=new sizeDAO();
                int a;
                a=rs.getInt(4);
                size s;
                s = si.doRetrieveById(a);
                p.setS(s);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return p;
    }


    public String doRetrieveDescById(int id){
        Statement st;
        ResultSet rs;
        product p=new product();
        String descr=null;

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeQuery("select descr from product where id="+id+";");

            while(rs.next()){
               descr=rs.getString(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return descr;
    }


    //carica da un form al database
    public void doSave(product p) {
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps = con.prepareStatement(
                    "INSERT INTO product (descr, price, size,image) VALUES(?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, p.getDesc());
            ps.setInt(2, p.getPrice());
            ps.setInt(3,p.getS().getId());
            ps.setString(4,p.getImage());

            if (ps.executeUpdate() != 1) {
                throw new RuntimeException("INSERT error.");
            }
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            int id = rs.getInt(1);
            p.setId(id);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }



    public List<product> doRetrieveAll(){
        ArrayList<product> pro=new ArrayList<>();

        Statement st;
        ResultSet rs;
        product p;

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeQuery("SELECT * FROM product Where 1=1");

            while(rs.next()){
                p=new product();
                p.setId(rs.getInt(1));
                p.setDesc(rs.getString(2));
                p.setPrice(rs.getInt(3));
                sizeDAO si=new sizeDAO();
                int a;
                a=rs.getInt(4);
                size s;
                s = si.doRetrieveById(a);
                p.setS(s);
                p.setImage(rs.getString(5));
                pro.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return pro;
    }



    public List<product> doRetrieveLatest(){
        ArrayList<product> pro=new ArrayList<>();

        Statement st;
        ResultSet rs;
        product p;

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeQuery("select * from product order by id DESC LIMIT 3;");

            while(rs.next()){
                p=new product();
                p.setId(rs.getInt(1));
                p.setDesc(rs.getString(2));
                p.setPrice(rs.getInt(3));
                sizeDAO si=new sizeDAO();
                int a;
                a=rs.getInt(4);
                size s;
                s = si.doRetrieveById(a);
                p.setS(s);
                p.setImage(rs.getString(5));
                pro.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return pro;
    }


    public void doUpdate(product c){
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="update product set descr='"+ c.getDesc() + "',price='" + c.getPrice() + "',size=" + c.getS().getName() + "where id=" + c.getId() + ";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    public void delete(product c){
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="delete * from product where id=" + c.getId() + ";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }


    public void deleteFromBuying(int idProd, int quantity){
        String desc=doRetrieveDescById(idProd);
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="delete from product where descr='"+ desc +"' LIMIT "+ quantity + ";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }


    public void deleteById(int idProd){
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="delete from product where id="+idProd+";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }


    public void doEdit(int idProd,String desc, int size, int price,String image){

        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="UPDATE product SET descr='"+desc+"',price="+price+",size="+size+",image='"+image+"' WHERE id="+idProd+";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }




    public List<size> doRetrieveSize(String descr){
        ArrayList<size> listaSize=new ArrayList<>();

        Statement st;
        ResultSet rs;
        size p;
        sizeDAO sDAO=new sizeDAO();

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeQuery("select distinct size from product where descr= '"+descr+"' order by size;");

            while(rs.next()){
                p=new size();
                p=sDAO.doRetrieveById(rs.getInt(1));
                listaSize.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listaSize;
    }


    public List<product> doRetrieveDesc(){
        ArrayList<product> pro=new ArrayList<>();

        Statement st;
        ResultSet rs;
        product p;

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeQuery("select distinct descr,image,price from product;");

            while(rs.next()){
                p=new product();
                p.setDesc(rs.getString(1));
                p.setImage(rs.getString(2));
                p.setPrice(rs.getInt(3));
               pro.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return pro;
    }


    public int doRetrieveCount(String descr){

        Statement st;
        ResultSet rs;
        int n=-1;

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeQuery("select count(*) from product where descr='"+descr+"';");

            while(rs.next()){
                n=rs.getInt(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return n;
    }


    public int doRetrieveCountSize(String descr,int idSize){

        Statement st;
        ResultSet rs;
        int n=-1;

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeQuery("select count(*) from product where descr='"+descr+"' and size="+idSize+";");

            while(rs.next()){
                n=rs.getInt(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return n;
    }


    /*public product doRetrieveByDescr(String descr){

        Statement st;
        ResultSet rs;
        product p;

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeQuery("SELECT * FROM product Where descr="+ descr +";");

            while(rs.next()){
                p=new product();
                p.setId(rs.getInt(1));
                p.setDesc(rs.getString(2));
                p.setPrice(rs.getInt(3));
                sizeDAO si=new sizeDAO();
                int a;
                a=rs.getInt(4);
                size s;
                s = si.doRetrieveById(a);
                p.setS(s);
                p.setImage(rs.getString(5));
                pro.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return pro;
    }*/



    //METODI CRUD: CREATE READ UPDATE DELETE,

}



