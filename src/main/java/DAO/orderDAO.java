package DAO;

import Model.cart;
import Model.order;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * A small table of banking customers for testing.
 */

public class orderDAO {

    /**
     * Finds the customer with the given ID.
     * Returns null if there is no match.
     */

    public order doRetrieveById(int id) {
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps =
                    con.prepareStatement("SELECT idadmin, iduser, stat, datareg, address FROM orders WHERE id=?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                order p = new order();
                p.setId(id);
                p.setIduser(rs.getInt(2));
                p.setIdadmin(rs.getInt(1));
                p.setStat(rs.getString(3));
                p.setDate(rs.getString(4));
                p.setAddress(rs.getString(5));
                return p;
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }




    public List<order> doRetrieveByUserId(int idUser){
        ArrayList<order> listaOrd=new ArrayList<>();

        Statement st;
        ResultSet rs;
        order p;

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeQuery("SELECT id, idadmin, iduser, stat, datareg, address FROM orders WHERE iduser="+idUser+";");

            while(rs.next()) {
                p = new order();
                p.setId(rs.getInt(1));
                p.setIduser(rs.getInt(3));
                p.setIdadmin(rs.getInt(2));
                p.setStat(rs.getString(4));
                p.setDate(rs.getString(5));
                p.setAddress(rs.getString(6));
                listaOrd.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listaOrd;
    }


   /* public List<order> doRetrieveByIdUser(int idUser) {
        ArrayList<order> orders=new ArrayList<>();
        Statement st;
        ResultSet rs;
        try (Connection con = ConPool.getConnection()) {
            st=con.createStatement();
            rs=st.executeQuery("SELECT id, idadmin, iduser, stat, datareg, address FROM orders WHERE iduser="+idUser+";");
            if (rs.next()) {
                order p = new order();
                p.setId(rs.getInt(1));
                p.setIduser(rs.getInt(3));
                p.setIdadmin(rs.getInt(2));
                p.setStat(rs.getString(4));
                p.setDate(rs.getString(5));
                p.setAddress(rs.getString(6));
                orders.add(p);
            }
            return orders;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }*/




    //carica da un form al database
    public int doSave(order o) {
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps = con.prepareStatement(
                    "INSERT INTO orders (iduser, stat, datareg, address) VALUES(?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, o.getIduser());
            ps.setString(2, o.getStat());
            ps.setString(3, o.getDate());
            ps.setString(4, o.getAddress());
            if (ps.executeUpdate() != 1) {
                throw new RuntimeException("INSERT error.");
            }
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            int id = rs.getInt(1);
            return id;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public List<order> doRetrieveAll(){
        ArrayList<order> orders=new ArrayList<>();

        Statement st;
        ResultSet rs;
        order p;

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeQuery("SELECT * FROM orders Where 1=1");

            while(rs.next()){
                p=new order();
                p.setId(rs.getInt(1));
                p.setIduser(rs.getInt(3));
                p.setIdadmin(rs.getInt(2));
                p.setStat(rs.getString(4));
                p.setDate(rs.getString(5));
                p.setAddress(rs.getString(6));
                orders.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }


    public void doUpdate(order c){
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="update orders set idadmin='"+ c.getIdadmin() + "',iduser='" + c.getIduser() + "',stat='" + c.getStat() + "',datareg='" + c.getDate() + "',address=" + c.getAddress() + "where id=" + c.getId() + ";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    public void delete(order c){
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="delete * from orders where id=" + c.getId() + ";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    //METODI CRUD: CREATE READ UPDATE DELETE,

}


