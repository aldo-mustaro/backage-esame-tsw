package DAO;

import Model.cart;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class cartDAO {

    public cart doRetrieveById(int id) {
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps =
                    con.prepareStatement("SELECT idprod, quantity, iduser FROM cart WHERE id=?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                cart p = new cart();
                p.setId(id);
                p.setIdprod(rs.getInt(1));
                p.setQuantity(rs.getInt(2));
                p.setIduser(rs.getInt(3));
                return p;
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public int getPrice(int id) {
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps =
                    con.prepareStatement("select price from product WHERE id=?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
            return rs.getInt(1);
            }
            return -1;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public String getDesc(int id) {
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps =
                    con.prepareStatement("select descr from product WHERE id=?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString(1);
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public String getSize(int id) { //DA RIVEDERE
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps =
                    con.prepareStatement("select size from product WHERE id=?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString(1);
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /*public List<cart> doRetrieveByUserId(int id) {
        ArrayList<cart> car=new ArrayList<>();

        Statement st;
        ResultSet rs;
        try (Connection con = ConPool.getConnection()) {
            st=con.createStatement();
            rs=st.executeQuery("select id, idprod, count(idprod) as qnty FROM cart WHERE iduser="+ id +" group by(idprod);");
            if (rs.next()) {
                cart p = new cart();
                p.setId(rs.getInt(1));
                p.setIdprod(rs.getInt(2));
                p.setQuantity(rs.getInt(3));
                car.add(p);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return car;
    }*/


    //carica da un form al database
    public int doSave(cart c) {
        int id=-1;
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps = con.prepareStatement(
                    "INSERT INTO cart (idprod, quantity, iduser) VALUES(?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, c.getIdprod());
            ps.setInt(2, c.getQuantity());
            ps.setInt(3, c.getIduser());
            if (ps.executeUpdate() != 1) {
                throw new RuntimeException("INSERT error.");
            }
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            id = rs.getInt(1);
            c.setId(id);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return id;
    }



    public List<cart> doRetrieveByUserId(int id){
        ArrayList<cart> carts=new ArrayList<>();

        Statement st;
        ResultSet rs;
        cart p;

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeQuery("select idprod, sum(quantity) as qnty FROM cart WHERE iduser="+ id +" group by(idprod);");

            while(rs.next()){
                p=new cart();
                p.setIdprod(rs.getInt(1));
                p.setQuantity(rs.getInt(2));
                carts.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return carts;
    }



    public void addQuantity(int idProd, int idUser){

        Statement st;
        int rs;

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeUpdate("insert into cart(idprod,quantity,iduser) value ("+ idProd + ",1,"+ idUser +");");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /*public int doCreate(cart c){
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps = con.prepareStatement(
                    "INSERT INTO cart (idprod, quantity, iduser) VALUES(?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);

            ps.setInt(1, c.getIdUtente());
            ps.setDouble(2,c.getTotale());
            ps.setInt(3,c.getNumeroProdotti());

            if (ps.executeUpdate() != 1) {
                throw new RuntimeException("INSERT error.");
            }

            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            int id = rs.getInt(1);
            c.setId(id);


            return id;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }*/


    public void doUpdate(cart c){
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="update cart set idprod='"+ c.getIdprod() + "',quantity='" + c.getQuantity() + "',iduser=" + c.getIduser() + "where id=" + c.getId() + ";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    public void delete(cart c){
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="delete * from cart where id=" + c.getId() + ";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    public void deleteById(int idProd, int idUser){
        int es=34323;
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="delete from cart where idprod=" + idProd + " and iduser= "+ idUser +" and quantity=1 LIMIT 1;";
             es=st.executeUpdate(query);
             if(es==0){
                 query="UPDATE cart SET quantity = quantity - 1 WHERE iduser="+ idUser +" and idprod="+ idProd +" LIMIT 1;";
                 es=st.executeUpdate(query);
             }
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }


    public void deleteByIdUser(int idUser){
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="delete from cart where iduser="+ idUser + ";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }


    public void deleteAll(){
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="delete from cart where iduser=1";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }



    public void deleteItem(int idProd, int idUser){
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="delete from cart where iduser="+ idUser + " and idProd=" + idProd + ";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }


    //METODI CRUD: CREATE READ UPDATE DELETE,

}


