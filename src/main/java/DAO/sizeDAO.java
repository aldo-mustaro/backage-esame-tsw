package DAO;

import Model.size;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class sizeDAO {


    public size doRetrieveById(int id) {
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps =
                    con.prepareStatement("SELECT descr FROM size WHERE id=?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                size p = new size();
                p.setId(id);
                p.setName(rs.getString(1));
                return p;
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public size doRetrieveByName(String name) {

        Statement st;
        ResultSet rs;
        size p=new size();

        try (Connection con = ConPool.getConnection()) {
            st=con.createStatement();
            rs=st.executeQuery("SELECT * FROM size WHERE descr='"+name+"';");

            if (rs.next()) {
                p.setName(name);
                p.setId(rs.getInt(1));
            }

            return p;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    //carica da un form al database
    public void doSave(size p) {
        try (Connection con = ConPool.getConnection()) {
            PreparedStatement ps = con.prepareStatement(
                    "INSERT INTO size (descr) VALUES(?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, p.getName());
            if (ps.executeUpdate() != 1) {
                throw new RuntimeException("INSERT error.");
            }
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            int id = rs.getInt(1);
            p.setId(id);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public List<size> doRetrieveAll(){
        ArrayList<size> orders=new ArrayList<>();

        Statement st;
        ResultSet rs;
        size p;

        try(Connection con= ConPool.getConnection()){
            st=con.createStatement();
            rs=st.executeQuery("SELECT * FROM size Where 1=1");

            while(rs.next()){
                p=new size();
                p.setId(rs.getInt(1));
                p.setName(rs.getString(2));
                orders.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }


    public void doUpdate(size c){
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="update ordersDetails set descr="+ c.getName() + "where id=" + c.getId() + ";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    public void delete(size c){
        try(Connection con= ConPool.getConnection()){
            Statement st= con.createStatement();
            String query="delete * from size where id=" + c.getId() + ";";
            st.executeUpdate(query);
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    //METODI CRUD: CREATE READ UPDATE DELETE,

}



