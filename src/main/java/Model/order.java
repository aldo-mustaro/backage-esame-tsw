package Model;

import java.util.ArrayList;
import java.util.GregorianCalendar;

public class order {
    private int id;
    private int idadmin;
    private int iduser;
    private String stat;
    private String date;
    private String address;
    private ArrayList<orderDetails> details;

    public order(){
       details=new ArrayList<>();
    }

    public order(int iduser, String stat, String date, String address) {
        this.iduser = iduser;
        this.stat = stat;
        this.date = date;
        this.address = address;
        details=new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdadmin() {
        return idadmin;
    }

    public void setIdadmin(int idadmin) {
        this.idadmin = idadmin;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ArrayList<orderDetails> getDetails() {
        return details;
    }

    public void addOrder(orderDetails oD){
        details.add(oD);
    }

    public void setDetails(ArrayList<orderDetails> details) {
        this.details = details;
    }
}
