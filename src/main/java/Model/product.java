package Model;

public class product {
    private int id;
    private int price;
    private String desc;
    private size s;
    private String image;

    public product(){}

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public product(int id, int price, String desc, size si) {
        this.id = id;
        this.price = price;
        this.desc = desc;
        this.s=si;
    }


    public size getS() {
        return s;
    }

    public void setS(size s) {
        this.s = s;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
