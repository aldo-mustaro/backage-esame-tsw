package Model;

public class User {
    private String fullname;
    private int id;
    private String username;
    private String email;
    private String address;
    private String password;

    public User(String fullname, int id, String username, String email, String address, String password) {
        this.fullname = fullname;
        this.id = id;
        this.username = username;
        this.email = email;
        this.address = address;
        this.password = password;
    }

    public User(){}

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}




