package Model;

public class orderDetails {
    private int id;
    private int idorders;
    private int idprod;
    private String desc;
    private int quantity;
    private int price;

    public orderDetails(){}

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public orderDetails(int idorders, int idprod, int quantity, int price, String desc) {
        this.idorders = idorders;
        this.idprod = idprod;
        this.quantity = quantity;
        this.price = price;
        this.desc=desc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdorders() {
        return idorders;
    }

    public void setIdorders(int idorders) {
        this.idorders = idorders;
    }

    public int getIdprod() {
        return idprod;
    }

    public void setIdprod(int idprod) {
        this.idprod = idprod;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
