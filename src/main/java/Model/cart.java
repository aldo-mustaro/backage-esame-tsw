package Model;

public class cart {
    private int id;
    private int idprod;
    private int quantity;
    private int iduser;


    public cart(){}

    public cart(int idprod, int quantity, int iduser) {
        this.idprod = idprod;
        this.quantity = quantity;
        this.iduser = iduser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdprod() {
        return idprod;
    }

    public void setIdprod(int idprod) {
        this.idprod = idprod;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }
}
